<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '9b8fe2b0b75283d6bd7c65069ae8fd2a6fb7f161',
        'name' => 'jose/jose',
        'dev' => true,
    ),
    'versions' => array(
        'benedmunds/codeigniter-ion-auth' => array(
            'pretty_version' => '4.x-dev',
            'version' => '4.9999999.9999999.9999999-dev',
            'type' => 'library',
            'install_path' => __DIR__ . '/../benedmunds/codeigniter-ion-auth',
            'aliases' => array(),
            'reference' => '19785a229677fbaeb5886783c30f820dba851d82',
            'dev_requirement' => false,
        ),
        'jose/jose' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '9b8fe2b0b75283d6bd7c65069ae8fd2a6fb7f161',
            'dev_requirement' => false,
        ),
    ),
);
