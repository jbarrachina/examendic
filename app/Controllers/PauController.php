<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

use App\Models\PauModel;
use App\Models\CiclosModel;
use Config\Services;

/**
 * Description of PauController
 *
 * @author jose
 */
class PauController extends BaseController {
    //put your code here
    protected $session;
    protected $auth;

    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
        //------------------------------------------------------------
        // Preload any models, libraries, etc, here.
        //------------------------------------------------------------
        $this->session = Services::session();
        $this->auth = new \IonAuth\Libraries\IonAuth();

    }


    public function index(){
        $pauModel = new PauModel();
        $data['titol'] = "Listado Solicitudes";
        $data['solicitudes'] = $pauModel
                ->select("nif, CONCAT(pau.apellido1,' ',pau.apellido2,', ',pau.nombre) as solicitante, email, pau.id, email, ciclos.nombre, ciclo, ciclos.id as id_ciclo, familia, tipo_tasa")
                ->join('ciclos','ciclos.id=pau.ciclo','LEFT')
                ->findAll();
        return view('solicitudes/lista',$data);
    }
    
    public function borrar($id){
        if ($this->auth->loggedIn() AND $this->auth->isAdmin()){
            $pauModel = new PauModel();
            $pauModel->delete($id);
            return redirect()->to('pauController');
        } else {
            echo "no tiene permiso";
        }
    }
    
    public function afegir(){
        helper(['form','myarray']);
        $pauModel = new PauModel();
        $ciclosModel = new CiclosModel();
        $data['titol'] = "Nueva Solicitud";
        if ($this->request->getMethod() == "post") { //viene de un formulario
             $reglas = $pauModel->getValidationRules();
             $reglas['email'].='|matches[email1]';
             $reglas['email1']='required|valid_email';
             if ($this->validate($reglas)){
                 $solicitud = $this->request->getPost();
                 unset($solicitud['email1']);
                 unset($solicitud['boton']);
                 $pauModel->insert($solicitud);
                 return redirect()->to('/pauController');
                 //print_r($solicitud);
             } else {
                 //mostrar formulario
                 
                 $data['errors'] = $this->validator;
             }
        } else { //viene de una URL
           //mostrar formulario 
        }
        $ciclos=$ciclosModel->select('id,nombre')
                ->where(['grado'=>'superior'])
                ->findAll();
        $data['ciclos'] = changeArray($ciclos, 'id', 'nombre');
        return view('solicitudes/form',$data);
    }
    
    public function afegir_cistella($id){
        $pauModel = new PauModel();
        $solicitud = $data['solicitudes'] = $pauModel
                ->select("nif, CONCAT(pau.apellido1,' ',pau.apellido2,', ',pau.nombre) as solicitante, email, pau.id, email, ciclos.nombre, ciclo, ciclos.id as id_ciclo, familia, tipo_tasa")
                ->join('ciclos','ciclos.id=pau.ciclo','LEFT')
                ->find($id);
        if ($this->session->has('solicitudes')) { 
            $solicitudes = $this->session->solicitudes;
            $solicitudes[]=$solicitud; //añadimos la nueva solicitud
            $this->session->set('solicitudes',$solicitudes);
        } else {//la primera vez
            $this->session->set('solicitudes',[$solicitud]); //un array de un elemento
        }
        print_r($this->session->solicitudes);
        return redirect()->to('pauController');
    }
    
    public function mostra_cistella(){
        $data['titol'] = "Listado Solicitudes en la Cesta";
        //no hace falta el modelo porque tenemos los datos en la variable sesión
        return view('solicitudes/lista_cistella',$data);
    }
    
    public function buidar(){
        $this->session->remove('solicitudes');
        return redirect()->to('pauController');
    }
}
