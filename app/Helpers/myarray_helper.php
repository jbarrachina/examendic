<?php
// convierte un array en tipo clave=>valor
function changeArray($array, $key, $value) {
    $new_array = [];
    foreach ($array as $element) {
        $new_array[$element[$key]] = $element[$value];
    }
    return $new_array;
}
