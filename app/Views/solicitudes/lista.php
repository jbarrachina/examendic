<?= $this->extend('layout/plantilla') ?>

<?= $this->section('content') ?>

<?php $session = \Config\Services::session(); ?>
<?php
    $auth = new \IonAuth\Libraries\IonAuth();
?>


<div class="d-flex flex-row-reverse bd-highlight">
    <div class="p-4 bd-highlight">
        <?php if ($auth->loggedIn()):?>
        <?php $user = $auth->user()->row();?>
        <span class="text-secondary">
            <?= $user->first_name.' '.$user->last_name?>
        </span>
        <a href="<?= site_url('auth/logout')?>">Salir</a>
        <?php else: ?>
        <a href="<?= site_url('auth/login')?>">Entrar</a>
        <?php endif; ?>
    </div>
    <div class="p-4 bd-highlight"><a href="<?=site_url('pauController/mostra_cistella')?>"><?=$session->has('solicitudes') ? count($session->solicitudes) : 'vacía'?></a></div>
    <?php if ($auth->loggedIn() AND ($auth->isAdmin() OR $auth->inGroup('secretaria'))):?>
        <div class="p-4 bd-highlight"><a href="<?= site_url('pauController/afegir') ?>" class="">Afegir</a></div>
    <?php endif;?>
</div>

<table class="table table-striped table-condensed" id="myTable">
    <thead>
        <?php if ($auth->loggedIn() AND ($auth->isAdmin() OR $auth->inGroup('secretaria'))):?>
        <th>NIE/NIF</th>
        <?php endif; ?>
        <th>Solicitante</th>
        <?php if ($auth->loggedIn() AND ($auth->isAdmin() OR $auth->inGroup('secretaria'))):?>
        <th>email</th>
        <?php endif; ?>
        <th>ciclo</th>
        <th>matrícula</th>
        <th></th>
    </thead>
    <?php foreach ($solicitudes as $solicitud): ?>
        <tr>
            <?php if ($auth->loggedIn() AND ($auth->isAdmin() OR $auth->inGroup('secretaria'))):?>
            <td><?= $solicitud['nif'] ?></td>
            <?php endif;?>
            <td><?= $solicitud['solicitante'] ?></td>
            <?php if ($auth->loggedIn() AND ($auth->isAdmin() OR $auth->inGroup('secretaria'))):?>
            <td><?= $solicitud['email'] ?></td>
            <?php endif;?>
            <td><?= $solicitud['nombre'] ?></td>
            <td>
                <?= $solicitud['tipo_tasa']==1 ? 'ordinaria' : ($solicitud['tipo_tasa']==3 ? 'gratuita' : 'semigratuita') ?>
            </td>
            <td>
                <a href="<?= site_url('pauController/afegir_cistella/'.$solicitud['id'])?>" 
                   class="btn btn-primary btn-sm">
                    Cistella
                </a>
                <?php if ($auth->loggedIn() AND $auth->isAdmin()):?>
                <a href="<?= site_url('pauController/borrar/'.$solicitud['id'])?>" 
                   class="btn btn-danger btn-sm" onclick="return confirm('Estás seguro de borrar la solicitud de <?= $solicitud['solicitante'] ?>')">
                    Borrar
                </a>
                <?php endif;?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>

<?= $this->endSection() ?>