<?= $this->extend('layout/plantilla') ?>

<?= $this->section('content') ?>

<?php $session = \Config\Services::session(); ?>


<table class="table table-striped table-condensed" id="myTable">
    <thead>
        <th>NIE/NIF</th>
        <th>Solicitante</th>
        <th>email</th>
        <th>ciclo</th>
        <th>matrícula</th>
    </thead>
    <?php foreach ($session->solicitudes as $solicitud): ?>
        <tr>
            <td><?= $solicitud['nif'] ?></td>
            <td><?= $solicitud['solicitante'] ?></td>
            <td><?= $solicitud['email'] ?></td>
            <td><?= $solicitud['nombre'] ?></td>
            <td>
                <?= $solicitud['tipo_tasa']==1 ? 'ordinaria' : ($solicitud['tipo_tasa']==3 ? 'gratuita' : 'semigratuita') ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
<div>
    <a href="<?= site_url('pauController') ?>" class="btn btn-primary btn-sm">
        Tornar
    </a>
    <a href="<?= site_url('pauController/buidar') ?>" class="btn btn-danger btn-sm">
        Buidar(Vaciar)
    </a>
    
</div>    

<?= $this->endSection() ?>